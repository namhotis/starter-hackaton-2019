import React from 'react'
import ReactDOM from 'react-dom'
import dab from './assets/img/download.jpeg'
import Button from './components/UI/Button/Button'
import './main.scss'

const App = () => {
    return (
        <main>
            <h1>Custom React Start</h1>
            <p>Hello World</p>
            <img src={dab} alt="" />
            <Button text="This is a button" />
        </main>
    )
}

ReactDOM.render(<App />, document.querySelector("#root"))